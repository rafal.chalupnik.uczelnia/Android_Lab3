package com.raven.android_lab3.DataAccess;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Result
{
    @PrimaryKey(autoGenerate = true)
    public int id;

    public int answer1;
    public int answer2;
    public int answer3;
    public int answer4;
    public int answer5;

    public int score;
}