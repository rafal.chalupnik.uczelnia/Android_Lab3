package com.raven.android_lab3.DataAccess;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

@Dao
public interface ResultDao
{
    @Query("SELECT AVG(Score) FROM Result")
    double getAverageScore();

    @Query("SELECT MAX(Score) FROM Result")
    int getBestScore();

    @Query("SELECT COUNT(*) FROM Result")
    int getGamesCount();

    @Query("SELECT MIN(Score) FROM Result")
    int getWorstScore();

    @Insert
    void insert(Result result);

    @Query("SELECT * FROM Result")
    Result[] selectAll();
}