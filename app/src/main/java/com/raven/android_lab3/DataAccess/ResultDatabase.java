package com.raven.android_lab3.DataAccess;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {Result.class}, version = 1)
public abstract class ResultDatabase extends RoomDatabase
{
    private static ResultDatabase instance_;

    public static ResultDatabase getDatabase(Context _context)
    {
        if (instance_ == null)
            instance_ = Room.databaseBuilder(_context, ResultDatabase.class, "resultDatabase").allowMainThreadQueries().build();
        return instance_;
    }

    public abstract ResultDao resultDao();
}