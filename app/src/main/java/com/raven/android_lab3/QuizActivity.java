package com.raven.android_lab3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.raven.android_lab3.Logic.Logic;

public class QuizActivity extends Activity implements View.OnClickListener
{
    RadioButton answer1RadioButton_;
    RadioButton answer2RadioButton_;
    RadioButton answer3RadioButton_;
    Button nextQuestionButton_;
    Button previousQuestionButton_;
    TextView questionCounterTextView_;
    ImageView questionImageView_;
    TextView questionTextView_;

    private Logic logic_;

    private int getQuestionImage(int _question)
    {
        switch (_question)
        {
            default: return R.drawable.ic_launcher_background;
        }
    }
    private void loadQuestion()
    {
        String[] answers = logic_.getQuestionAnswers();
        answer1RadioButton_.setText(answers[0]);
        answer2RadioButton_.setText(answers[1]);
        answer3RadioButton_.setText(answers[2]);
        answer1RadioButton_.setChecked(false);
        answer1RadioButton_.setChecked(false);
        answer1RadioButton_.setChecked(false);

        questionCounterTextView_.setText(logic_.getActualQuestion() + 1 + " / " + logic_.getQuestionCount());
        questionImageView_.setImageResource(getQuestionImage(logic_.getActualQuestion()));
        questionTextView_.setText(logic_.getQuestion());

        previousQuestionButton_.setClickable(logic_.getActualQuestion() != 0);
    }

    @Override
    public void onClick(View _view)
    {
        int answer = -1;
        if (answer1RadioButton_.isChecked())
            answer = 0;
        else if (answer2RadioButton_.isChecked())
            answer = 1;
        else if (answer3RadioButton_.isChecked())
            answer = 2;

        if (answer != -1)
        {
            logic_.setAnswer(answer);

            if (_view == nextQuestionButton_)
            {
                if (logic_.getActualQuestion() == logic_.getQuestionCount() - 1)
                {
                    logic_.saveAnswers();
                    Intent intent = new Intent(getApplicationContext(), ResultActivity.class);
                    intent.putExtra("score", logic_.getScore());
                    intent.putExtra("max", logic_.getQuestionCount());
                    startActivity(intent);
                }
                else
                {
                    logic_.nextQuestion();
                    loadQuestion();
                }
            }
            else if (_view == previousQuestionButton_)
            {
                logic_.previousQuestion();
                loadQuestion();
            }
        }
    }

    @Override
    protected void onCreate(Bundle _savedInstanceState)
    {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_quiz);

        answer1RadioButton_ = findViewById(R.id.answer1RB);
        answer2RadioButton_ = findViewById(R.id.answer2RB);
        answer3RadioButton_ = findViewById(R.id.answer3RB);

        nextQuestionButton_ = findViewById(R.id.nextQuestionBT);
        nextQuestionButton_.setOnClickListener(this);

        previousQuestionButton_ = findViewById(R.id.previousQuestionBT);
        previousQuestionButton_.setOnClickListener(this);

        questionCounterTextView_ = findViewById(R.id.questionCounterTV);
        questionImageView_ = findViewById(R.id.questionImageIV);
        questionTextView_ = findViewById(R.id.questionTextTV);

        logic_ = new Logic(getApplicationContext());

        loadQuestion();
    }
}