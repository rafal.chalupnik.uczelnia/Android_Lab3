package com.raven.android_lab3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener
{
    Button startQuizButton_;
    Button statisticsButton_;

    @Override
    public void onClick(View _view)
    {
        Intent intent = null;

        if (_view == startQuizButton_)
            intent = new Intent(getApplicationContext(), QuizActivity.class);
        else if (_view == statisticsButton_)
            intent = new Intent(getApplicationContext(), StatisticsActivity.class);

        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle _savedInstanceState)
    {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_main);

        startQuizButton_ = findViewById(R.id.startQuizBT);
        startQuizButton_.setOnClickListener(this);

        statisticsButton_ = findViewById(R.id.statisticsBT);
        statisticsButton_.setOnClickListener(this);
    }
}