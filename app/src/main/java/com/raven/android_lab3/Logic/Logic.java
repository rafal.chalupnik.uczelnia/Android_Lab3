package com.raven.android_lab3.Logic;

import android.content.Context;

import com.raven.android_lab3.DataAccess.Result;
import com.raven.android_lab3.DataAccess.ResultDao;
import com.raven.android_lab3.DataAccess.ResultDatabase;
import com.raven.android_lab3.Statistics;

public class Logic
{
    private final Hardcode hardcode_;
    private final ResultDao resultDao_;

    private int actualQuestion_ = 0;
    private final int[] givenAnswers_;
    private final int questionCount_ = 5;

    public Logic(Context _context)
    {
        givenAnswers_ = new int[questionCount_];
        hardcode_ = new Hardcode();
        resultDao_ = ResultDatabase.getDatabase(_context).resultDao();
    }

    public int getActualQuestion() { return actualQuestion_; }
    public String getQuestion() { return hardcode_.questions[actualQuestion_]; }
    public String[] getQuestionAnswers() { return hardcode_.answers[actualQuestion_]; }
    public int getQuestionCount() { return questionCount_; }
    public int getScore()
    {
        int score = 0;
        for (int i = 0; i < questionCount_; i++)
        {
            if (hardcode_.rightAnswers[i] == givenAnswers_[i])
                score++;
        }
        return score;
    }
    public Statistics getStatistics()
    {
        Statistics statistics = new Statistics();
        statistics.averageScore = resultDao_.getAverageScore();
        statistics.bestScore = resultDao_.getBestScore();
        statistics.gameCount = resultDao_.getGamesCount();
        statistics.worstScore = resultDao_.getWorstScore();
        return statistics;
    }
    public void nextQuestion()
    {
        if (actualQuestion_ != questionCount_ - 1)
            actualQuestion_++;
    }
    public void saveAnswers()
    {
        Result result = new Result();
        result.answer1 = givenAnswers_[0];
        result.answer1 = givenAnswers_[1];
        result.answer1 = givenAnswers_[2];
        result.answer1 = givenAnswers_[3];
        result.answer1 = givenAnswers_[4];
        result.score = getScore();

        resultDao_.insert(result);
    }
    public void setAnswer(int _answer) { givenAnswers_[actualQuestion_] = _answer; }
    public void previousQuestion()
    {
        if (actualQuestion_ != 0)
            actualQuestion_--;
    }
}