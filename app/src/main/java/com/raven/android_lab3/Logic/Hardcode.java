package com.raven.android_lab3.Logic;

public class Hardcode
{
    public String[][] answers = new String[5][3];
    public String[] questions = new String[5];
    public int[] rightAnswers = new int[5];

    public Hardcode()
    {
        questions[0] = "Ile jest równe 2+2?";
        answers[0][0] = "3";
        answers[0][1] = "4";
        answers[0][2] = "5";
        rightAnswers[0] = 1;

        questions[1] = "Co jest najlepsze?";
        answers[1][0] = "C#";
        answers[1][1] = "Java";
        answers[1][2] = "Pascal";
        rightAnswers[1] = 0;

        questions[2] = "Wybierz prawidłową odpowiedź:";
        answers[2][0] = "Zła odpowiedź";
        answers[2][1] = "Kolejna zła odpowiedź";
        answers[2][2] = "Prawidłowa odpowiedź";
        rightAnswers[2] = 2;

        questions[3] = "Jak się nazywa najnowszy system na Maka?";
        answers[3][0] = "High Sierra";
        answers[3][1] = "Yosemite";
        answers[3][2] = "Sierra";
        rightAnswers[3] = 0;

        questions[4] = "Jakie lotnisko jest największe?";
        answers[4][0] = "Pcim Dolny";
        answers[4][1] = "Frankfurt";
        answers[4][2] = "Pjongjang";
        rightAnswers[4] = 1;
    }
}