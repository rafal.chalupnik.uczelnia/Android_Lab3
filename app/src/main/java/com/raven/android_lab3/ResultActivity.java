package com.raven.android_lab3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ResultActivity extends Activity implements View.OnClickListener
{
    Button againButton_;
    Button returnButton_;
    TextView scoreTextView_;

    @Override
    public void onClick(View _view)
    {
        Intent intent = null;

        if (_view == againButton_)
            intent = new Intent(getApplicationContext(), QuizActivity.class);
        else if (_view == returnButton_)
            intent = new Intent(getApplicationContext(), MainActivity.class);

        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle _savedInstanceState)
    {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_result);

        againButton_ = findViewById(R.id.againBT);
        returnButton_ = findViewById(R.id.returnBT);
        scoreTextView_ = findViewById(R.id.scoreTV);

        Intent intent = getIntent();
        int score = intent.getIntExtra("score", -1);
        int max = intent.getIntExtra("max", -1);

        String toShow = score + " / " + max;
        scoreTextView_.setText(toShow);

        againButton_.setOnClickListener(this);
        returnButton_.setOnClickListener(this);
    }
}
