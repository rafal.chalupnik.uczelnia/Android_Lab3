package com.raven.android_lab3;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.raven.android_lab3.Logic.Logic;

public class StatisticsActivity extends Activity
{
    TextView averageScoreTextView_;
    TextView bestScoreTextView_;
    TextView gamesMadeTextView_;
    TextView worstScoreTextView_;

    private Logic logic_;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        averageScoreTextView_ = findViewById(R.id.averageScoreTV);
        bestScoreTextView_ = findViewById(R.id.bestScoreTV);
        gamesMadeTextView_ = findViewById(R.id.gamesMadeTV);
        worstScoreTextView_ = findViewById(R.id.worstScoreTV);

        logic_ = new Logic(getApplicationContext());

        Statistics statistics = logic_.getStatistics();

        averageScoreTextView_.setText("Średni wynik: " + statistics.averageScore);
        bestScoreTextView_.setText("Najlepszy wynik: " + statistics.bestScore);
        gamesMadeTextView_.setText("Rozegranych gier: " + statistics.gameCount);
        worstScoreTextView_.setText("Najgorszy wynik: " + statistics.worstScore);
    }
}
